(require 'package)
(setq package-enable-at-startup nil)
(add-to-list 'package-archives
	     '("melpa" . "https://melpa.org/packages/"))
(package-initialize)

;; use-package
(eval-when-compile
  (require 'use-package))

(org-babel-load-file (expand-file-name "~/.emacs.d/inits/commoninit.org"))
(org-babel-load-file (expand-file-name "~/.emacs.d/inits/serverinit.org"))
